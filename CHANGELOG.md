Changelog
=========

v1.1.1
------

- Mobile support is compatible with local_mobile

v1.1.0
------

- Moodle Mobile app support
- Fixed invalid link to profile in log
- Badge theme setting was not backed up
- Badges resolution increased
- Minor bug fixes and improvements
